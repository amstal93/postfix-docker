# Postfix in Docker

## Description

Pure [Postfix][postfix] installation in a Docker image based on the
[Postfix package][postfix-package] in Debian 11 Bullseye.
Fully customizable via mounted volumes for usage on a Virtual Private Server.

[postfix]: http://www.postfix.org/
[postfix-package]: https://packages.debian.org/bullseye/postfix

## Docker Repository

See the [registry page][registry].

[registry]: https://gitlab.com/moasda/postfix-docker/container_registry

## Run with Docker Compose

Example for `docker-compose.yml`:
```yaml
version: '3.8'

services:
  postfix:
    image: registry.gitlab.com/moasda/postfix-docker/postfix-docker:3.5.6-1-b1-2022-03-30
    container_name: postfix
    hostname: mx.myhostname.com
    restart: always
    networks:
      - web
    ports:
      - "25:25"
    volumes:
      - /etc/postfix:/etc/postfix
      - /var/spool/postfix:/var/spool/postfix
      - /var/log/postfix.log:/var/log/postfix.log

networks:
  web:
    external: true
```

## Build

1. Create repository tag, e.g. `2022-03-30`
2. GitLab CI will create a new docker image named `postfix-docker:<postfix version>-<tag name>`, e.g. `postfix-docker:3.5.6-1-b1-2022-03-30`
3. Use Docker image, see Docker Compose example above

## License

The files in this repository are licensed under GPL-3.0-only &ndash; see the
[LICENSE](LICENSE) file for details.
